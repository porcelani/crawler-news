## Crawler News

## Execute
-  Compile  `sbt compile`
-  Test  `sbt test`
-  Coverage  `sbt test-coverage`
-  crawler  `sbt crawler`
-  graphql  `sbt graphql`

## API:
### /health
```
curl -X GET -H "Content-Type: application/json" \
 http://localhost:8080/health
---
200 OK
"OK"
```

### /graphql
```
curl -X POST -H "Content-Type: application/json" \
 --data '{ "query": "{ news { title link } } " }' \
 http://localhost:8080/graphql
---
200 OK
{
   "news":[
      {
         "title":xxx,
         "link":"xxx"
      },
      {
         "title":xxx,
         "link":"xxx"
      },
      ...
   ]
}
```

## References:
- [Documents](doc)

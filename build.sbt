import Dependencies.Libraries._

name := "crawler-news"
version := "0.1"
scalaVersion := "2.13.4"

addCommandAlias("fmt", "all scalafmtSbt scalafmtAll")
addCommandAlias("test-coverage", ";clean ;coverage ;test ;coverageAggregate")
addCommandAlias("crawler", "crawler/runMain com.crawler.news.CrawlerApp")
addCommandAlias("graphql", "graphql/runMain com.crawler.news.ServerApp")

lazy val model = project
  .in(file("modules/model"))
  .settings(
    libraryDependencies ++= circe ++ h2 ++ quill ++
      scalatest
  )

lazy val crawler = project
  .in(file("modules/crawler"))
  .settings(
    libraryDependencies ++= zio ++ zioLogging ++ logging ++ zioConfig ++ circe ++ jackson ++ scraper ++
      scalatest ++ zioTest,
    testFrameworks := Seq(new TestFramework("zio.test.sbt.ZTestFramework"))
  )
  .dependsOn(model)

lazy val graphql = project
  .in(file("modules/graphql"))
  .settings(
    libraryDependencies ++=
      zio ++ sttp ++ zioLogging ++ logging ++ zioConfig ++ circe ++ jackson ++ sangria ++ http4s ++
        scalatest ++ zioTest,
    testFrameworks := Seq(new TestFramework("zio.test.sbt.ZTestFramework"))
  )
  .dependsOn(model)

coverageMinimumStmtTotal := 85
coverageFailOnMinimum := true

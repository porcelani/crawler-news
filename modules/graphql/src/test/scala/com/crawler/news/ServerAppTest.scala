package com.crawler.news

import com.crawler.news.ServerApp.AppService
import com.crawler.news.repository.deleteAll
import io.circe.Json
import io.circe.literal.JsonStringContext
import org.http4s._
import org.http4s.circe._
import org.http4s.implicits._
import zio._
import zio.interop.catz._
import zio.test.Assertion._
import zio.test.TestAspect._
import zio.test._
object ServerAppTest extends DefaultRunnableSpec {
  implicit val jsonEncoder: EntityEncoder[Task, Json] = jsonEncoderOf
  private val query                                   = json"""{ "query": "{ news { title link } } "}"""

  override def spec: ZSpec[Any, Throwable] =
    suite("routes suite")(
      testM("root request returns NotFound") {
        for {
          response <- AppService.service.run(Request[Task](Method.GET, uri"/"))
        } yield assert(response.status)(equalTo(Status.NotFound))
      },
      testM("health request returns Ok") {
        for {
          response <- AppService.service.run(Request[Task](Method.GET, uri"/health"))
          body     <- response.body.through(fs2.text.utf8Decode).compile.string
        } yield assert(response.status)(equalTo(Status.Ok)) &&
          assert(body)(equalTo(""""OK""""))
      },
      testM("graphql request returns Ok") {
        for {
          _        <- ZIO.effect(deleteAll())
          response <- AppService.service.run(Request[Task](Method.POST, uri"/graphql").withEntity(query))
          body     <- response.body.through(fs2.text.utf8Decode).compile.string
        } yield assert(response.status)(equalTo(Status.Ok)) &&
          assert(body)(equalTo(""""{\"data\":{\"news\":[]}}""""))
      }
    ) @@ sequential

}

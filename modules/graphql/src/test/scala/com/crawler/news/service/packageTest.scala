package com.crawler.news.service

import com.crawler.news.domain.Headlines
import com.crawler.news.repository.{ deleteAll, insert }
import org.scalatest.BeforeAndAfter
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.concurrent.ScalaFutures._
import org.scalatest.funsuite.AnyFunSuite

import scala.concurrent.duration._

class packageTest extends AnyFunSuite with BeforeAndAfter {

  implicit val patienceConfig: ScalaFutures.PatienceConfig = PatienceConfig(10.second)
  val query =
    """
    {
      news {
        link
        title
      }
    }
    """

  before {
    deleteAll()
  }

  test("should verify graphql return from a empty repository") {
    val futureResult = getGraphql(Option(query))

    whenReady(futureResult)(result => assert(result == """{"data":{"news":[]}}"""))
  }

  test("should verify graphql return some register") {
    val headlines = Headlines("https://example.com", "Example Title")
    insert(headlines)

    val futureResult = getGraphql(Option(query))

    whenReady(futureResult)(result =>
      assert(
        result == """{"data":{"news":[{"link":"https://example.com","title":"Example Title"}]}}"""
      )
    )
  }
}

package com.crawler.news.service

import com.crawler.news.domain.Headlines
import com.crawler.news.repository
import sangria.schema._

object SchemaDefinition {
  val HeadlinesType = ObjectType(
    "Headlines",
    fields[Unit, Headlines](
      Field("link", StringType, resolve = _.value.link),
      Field("title", StringType, resolve = _.value.title)
    )
  )

  val HeadlineListType = ObjectType(
    "HeadlinesList",
    fields[Unit, Unit](
      Field(
        "news",
        ListType(HeadlinesType),
        resolve = _ => repository.all()
      )
    )
  )

  val schema = Schema(HeadlineListType)
}

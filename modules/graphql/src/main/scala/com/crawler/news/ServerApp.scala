package com.crawler.news

import com.crawler.news.service.getGraphql
import io.circe.Json
import io.circe.optics.JsonPath.root
import org.http4s._
import org.http4s.circe.CirceEntityCodec.circeEntityEncoder
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import org.http4s.implicits._
import org.http4s.server.blaze.BlazeServerBuilder
import zio._
import zio.console._
import zio.interop.catz._
import zio.interop.catz.implicits._

import scala.concurrent.ExecutionContext.Implicits.global

object ServerApp extends zio.App {
  object AppService {

    private val dsl = Http4sDsl[Task]
    import dsl._

    val service = HttpRoutes
      .of[Task] {
        case GET -> Root / "health" => Ok("OK")

        case request @ POST -> Root / "graphql" =>
          request.as[Json].flatMap { body =>
            val query       = root.query.string.getOption(body)
            val queryResult = getGraphql(query)
            ZIO.fromFuture(_ => queryResult.map(response => Ok(response))).flatten
          }
      }
      .orNotFound
  }

  def run(args: List[String]): zio.URIO[zio.ZEnv, ExitCode] =
    ZIO
      .runtime[ZEnv]
      .flatMap { implicit runtime =>
        BlazeServerBuilder[Task](runtime.platform.executor.asEC)
          .bindHttp(8080, "localhost")
          .withHttpApp(AppService.service)
          .resource
          .toManagedZIO
          .useForever
          .foldCauseM(
            err => putStrLn(err.prettyPrint).as(ExitCode.failure),
            _ => ZIO.succeed(ExitCode.success)
          )
      }
}

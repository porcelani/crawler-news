package com.crawler.news
import com.crawler.news.service.SchemaDefinition.schema
import sangria.execution._
import sangria.marshalling.circe._
import sangria.parser.QueryParser

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

package object service {

  def getGraphql(query: Option[String]): Future[String] = {
    val document = QueryParser.parse(query.getOrElse("")).get

    val queryResult = Executor.execute(schema, document)

    queryResult.map(res => res.noSpaces)
  }
}

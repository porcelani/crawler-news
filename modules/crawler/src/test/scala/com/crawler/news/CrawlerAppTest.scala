package com.crawler.news

import com.crawler.news.CrawlerApp.crawlerApp
import zio.Task
import zio.test.Assertion.isTrue
import zio.test.{ assert, DefaultRunnableSpec }

object CrawlerAppTest extends DefaultRunnableSpec {
  def spec =
    suite("CrawlerAppTest")(
      testM("should crawler and save all headlines") {
        for {
          _ <- Task.effect(repository.deleteAll())
          _ <- crawlerApp
        } yield assert(repository.all().nonEmpty)(isTrue)
      }
    )
}

package com.crawler.news.service

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source.fromResource

class packageTest extends AnyFunSuite {

  test("should verify crawler return") {
    val headlines = getHeadlinesList()

    assert(headlines.nonEmpty)
  }

  test("should not find headlines in a tag") {
    val html      = fromResource("a.example.nytimes.com.html").mkString
    val headlines = getHeadlinesList(browser.parseString(html))
    assert(headlines.isEmpty)
  }

  test("should find headlines in h2 tag") {
    val html      = fromResource("h2.example.nytimes.com.html").mkString
    val headlines = getHeadlinesList(browser.parseString(html))

    assert(headlines.size == 1)
  }

  test("should find headlines in h3 tag") {
    val html      = fromResource("h3.example.nytimes.com.html").mkString
    val headlines = getHeadlinesList(browser.parseString(html))

    assert(headlines.size == 1)
  }

  test("should find headlines in filter duplicated href") {
    val html      = fromResource("a.duplicated.example.nytimes.com.html").mkString
    val headlines = getHeadlinesList(browser.parseString(html))

    assert(headlines.size == 1)
  }

  test("should find all headlines in entire html") {
    val html      = fromResource("example.nytimes.com.html").mkString
    val headlines = getHeadlinesList(browser.parseString(html))

    assert(headlines.size == 41)
  }
}

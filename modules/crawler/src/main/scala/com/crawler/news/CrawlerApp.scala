package com.crawler.news

import com.crawler.news.service.getHeadlinesList
import zio.Task

object CrawlerApp extends zio.App {

  def run(args: List[String]) =
    crawlerApp.exitCode

  val crawlerApp =
    for {
      headlineList <- Task.effect(getHeadlinesList())
      _            <- Task.effect(headlineList.foreach(repository.insert))
    } yield ()
}

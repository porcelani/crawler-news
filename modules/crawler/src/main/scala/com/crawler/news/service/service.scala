package com.crawler.news

import com.crawler.news.domain.Headlines
import net.ruippeixotog.scalascraper.browser.{ Browser, JsoupBrowser }
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.model.Element
import net.ruippeixotog.scalascraper.scraper.ContentExtractors.{ allText, elements }

package object service {
  val baseUrl          = "https://www.nytimes.com"
  val browser: Browser = JsoupBrowser()

  def getHeadlinesList(doc: browser.DocumentType = browser.get(baseUrl)) = {
    val linksElements = doc >> elements("a")
    val headlinesList = linksElements
      .map(element => Headlines(link = element.attr("href"), title = getTagH(element)))
      .filter(headline => headline.title.nonEmpty)
      .toList
      .groupBy(_.link)
      .map(_._2.head)

    headlinesList
  }

  private def getTagH(element: Element) =
    List(element >> allText("h2"), element >> allText("h3")).foldLeft("")(_ + _)
}

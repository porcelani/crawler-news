CREATE TABLE IF NOT EXISTS headlines (
  link VARCHAR NOT NULL,
  title VARCHAR NOT NULL
);


ALTER TABLE headlines
    ADD CONSTRAINT headlines_pkey PRIMARY KEY (link);

COMMIT;


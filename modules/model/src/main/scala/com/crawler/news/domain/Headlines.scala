package com.crawler.news.domain

case class Headlines(
  link: String,
  title: String
)

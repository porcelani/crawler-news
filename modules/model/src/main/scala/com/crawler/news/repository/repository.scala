package com.crawler.news

import com.crawler.news.domain.Headlines
import io.getquill.{ H2JdbcContext, SnakeCase }
import org.h2.jdbc.JdbcSQLIntegrityConstraintViolationException

package object repository {
  lazy val ctx = new H2JdbcContext(SnakeCase, "ctx")

  import ctx._

  def insert(headlines: Headlines) =
    try {
      ctx.run(quote {
        query[Headlines].insert(lift(headlines))
      })
    } catch {
      case e: JdbcSQLIntegrityConstraintViolationException => println("record already included.")
    }

  def all() =
    ctx.run(quote {
      query[Headlines].filter(_ => true)
    })

  def deleteAll() =
    ctx.run(quote {
      query[Headlines].delete
    })

}

package com.crawler.news.repository

import com.crawler.news.domain.Headlines
import org.scalatest.BeforeAndAfter
import org.scalatest.funsuite.AnyFunSuite

import scala.language.postfixOps

class packageTest extends AnyFunSuite with BeforeAndAfter {
  before {
    deleteAll()
  }

  test("should verify repository operations ") {
    val headlines = Headlines("https://example.com", "Example Title")
    insert(headlines)

    val headlinesList = all()
    assert(headlinesList == List(headlines))

    deleteAll()
    val emptyList = all()
    assert(emptyList == List())
  }
}

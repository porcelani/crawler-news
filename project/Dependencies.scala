import sbt._

object Dependencies {

  private object Versions {
    val zio        = "1.0.7"
    val zioConfig  = "1.0.0"
    val zioLogging = "0.4.0"
    val circe      = "0.13.0"
    val sttp       = "2.2.9"
    val log4j      = "2.13.3"
    val disruptor  = "3.4.2"
    val jackson    = "2.12.0"
    val scraper    = "2.2.1"
    val h2         = "1.4.199"
    val quill      = "3.7.1"
    val http4s     = "1.0.0-M4"
  }

  object Libraries {
    val zio = Seq(
      "dev.zio" %% "zio"              % Versions.zio,
      "dev.zio" %% "zio-streams"      % Versions.zio,
      "dev.zio" %% "zio-interop-cats" % "2.1.4.0"
    )

    lazy val zioConfig = Seq(
      "dev.zio" %% "zio-config-magnolia" % Versions.zioConfig,
      "dev.zio" %% "zio-config-typesafe" % Versions.zioConfig
    )

    val zioLogging = Seq(
      "dev.zio" %% "zio-logging"       % Versions.zioLogging,
      "dev.zio" %% "zio-logging-slf4j" % Versions.zioLogging
    )

    val circe = Seq(
      "io.circe" %% "circe-core"    % Versions.circe,
      "io.circe" %% "circe-generic" % Versions.circe,
      "io.circe" %% "circe-optics"  % Versions.circe,
      "io.circe" %% "circe-literal" % Versions.circe
    )

    val sttp = Seq(
      "com.softwaremill.sttp.client" %% "async-http-client-backend-zio" % Versions.sttp,
      "com.softwaremill.sttp.client" %% "circe"                         % Versions.sttp
    )

    val logging = Seq(
      "org.apache.logging.log4j" % "log4j-core"       % Versions.log4j,
      "org.apache.logging.log4j" % "log4j-slf4j-impl" % Versions.log4j,
      "com.lmax"                 % "disruptor"        % Versions.disruptor
    )

    val jackson = Seq("com.fasterxml.jackson.core" % "jackson-databind" % Versions.jackson)

    val scraper = Seq("net.ruippeixotog" %% "scala-scraper" % Versions.scraper)

    val h2 = Seq("com.h2database" % "h2" % Versions.h2)

    val quill = Seq("io.getquill" %% "quill-jdbc" % Versions.quill)

    val sangria = Seq(
      "org.sangria-graphql" %% "sangria"       % "2.1.3",
      "org.sangria-graphql" %% "sangria-circe" % "1.3.1"
    )

    val http4s = Seq(
      "org.http4s" %% "http4s-blaze-server" % Versions.http4s,
      "org.http4s" %% "http4s-dsl"          % Versions.http4s,
      "org.http4s" %% "http4s-circe"        % Versions.http4s
    )

    // Tests
    val scalatest = Seq("org.scalatest" %% "scalatest-funsuite" % "3.2.7" % Test)

    val zioTest = Seq(
      "dev.zio" %% "zio-test"     % Versions.zio % "test",
      "dev.zio" %% "zio-test-sbt" % Versions.zio % "test"
    )
  }
}
